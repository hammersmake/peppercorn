#!/usr/bin/env python
# coding: utf-8

# In[3]:


import getpass

import os

import sqlite3

import arrow


# In[4]:


objda = {0 : {'code' : '6mu', 
              'objection' :  'It is unreasonable to charge this as a separate item on the basis of rounding-up to a whole six-minute unit.'
             },
        1 : {'code' : 'adm',
            'objection' : 'The work is administrative in character and should not be charged at a qualified lawyer’s rate.'},
        2 : {'code' : 'billing', 
            'objection' : 'This item contravenes the prohibition on a law practice charging a client or third party payer for preparing or giving a bill (s 191).'},
        3 : {'code' : 'disc',
            'objection' : 'This item claims to charge for compliance with the statutory obligation of disclosure.  Such disclosure is not the provision of legal services.',
            },
        4 : {'code' : 'dup', 'objection' : 'The work has been done by another person and a second charge for it is not fair and reasonable.'},
        5 : {'code' : 'exp', 'objection' : 'The level of experitse of the person who did the work was higher than the work reasonably justified.  It is unreasonable to charge for the work at that person’s rate.'},
        6 : {'code' : 'fax', 'objection' : 'It is unreasonable to charge a separate fee for sending/receiving a facsimile transmission.'},
        7 : {'code' : 'hr', 'objection' : 'The hourly or other periodic rate claimed is unreasonable.'},
        8: {'code' : 'inst', 'objection' : 'This item was outside the scope of the work that the law practice was instructed to perform.'}, 
        9: {'code' : 'nr', 'objection' : 'It was not reasonable to do the work, or it was not reasonable to include a claim for doing the work.'},
        10: {'code' : 'pc', 'objection' : 'The photocopying rate is unreasonable.'},
        11 : {'code' : 'scope', 'objection' : 'This item is outside the scope of the costs order.  [Only relevant in the assessment of ordered costs.]'},
        12: {'code' : 'tim', 'objection' : 'The work should not reasonably have taken the time claimed or charged.'},
        13: {'code' : 'travel', 'objection' : 'The rate charged for travelling time is unreasonable.'}}


# In[5]:


timnow = arrow.now()


# In[6]:


timnow.timestamp


# In[7]:


timenow = arrow.now()


def createdb(namedatabase):
        #Creates a database with the table buttonpush. Give it the name of database.
        conn = sqlite3.connect('{}.db'.format(namedatabase))
        c = conn.cursor()

        c.execute('''CREATE TABLE objentry
                     (date text, username text, length text)''')
        
        conn.close()

def objcreate(namedatabase, username, lentime):
    #make request of objections based on 0-13 linked to code
    #and objection. 
    #attach doc
    conn = sqlite3.connect('{}.db'.format(namedatabase))
    
    c = conn.cursor()
    timenow = arrow.now()


    c.execute("INSERT INTO objentry VALUES ('{}','{}', {})".format(timenow.timestamp,
                                                                        username, lentime))

    conn.commit()
    conn.close()


def readata(namedatabase):
    #This reads the database. Give it name of database.
    conn = sqlite3.connect('{}.db'.format(namedatabase))
    c = conn.cursor()

    c.execute('SELECT * FROM objentry')
    #conn.close()
    return(c.fetchall())
    conn.close()


# In[8]:


def readone(namedatabase):
    conn = sqlite3.connect('{}.db'.format(namedatabase))
    c = conn.cursor()

    c.execute('SELECT * FROM objentry ORDER BY date DESC LIMIT 1;')
    #conn.close()
    return(c.fetchall())
    conn.close()
    
    


# In[ ]:





# In[9]:


#readone('imoti')


# In[ ]:





# In[10]:


#createdb('imoti')


# In[11]:


#createdb('iobjs')


# In[12]:


#objcreate('imoti', 'william', 358)


# In[ ]:





# In[13]:


#getpass.getuser()


# In[14]:


def whatcodes(watcde):
    for wspl in watcde:
        print(objda[wspl])


# In[15]:


#codelis = [0,1,3,8]


# In[16]:


#codelis


# In[17]:


#whatcodes(codelis)


# In[18]:


#timnow.timestamp


# In[19]:


#resultdic = dict({'codelis' : codelis, 'datetime' : timnow.timestamp, 'username' : getpass.getuser()})


# In[20]:


#resultdic


# In[21]:


#for wspl in whatcodes.split(','):
#    print(wspl)
#    print(objda[int(wspl)])
#    resultdic.update({})


#     Forms by subject  
# 
#     Administrative & Industrial law​​​​​​​​​​
#     Adoption
#     Bail & Crime
#     ​Civil appeals (Court of Appeal)
#     Common Law General
#     Corporations law
#     Costs assessment scheme
#     Criminal appeal (Court of Criminal Appeal) 
#     Document access, copying and search reports
#     Family Provision and Succession
#     Fee postponement, waiver, refund and reduction
#     JusticeLink crime
#     Listing Services (incl. video and telephone links)
#     Media
#     Mediation
#     Probate
#     Subpoenas
#     Transcript order forms​
# 

# In[22]:


formsub = dict({0 : {'name' : 'adminindust', 'file' : 'http://www.supremecourt.justice.nsw.gov.au/Documents/01summonstoshowcause2016.doc',
                    'docname' : '01summonstoshowcause2016'},
               1 : {'name' : 'corporation', 'file' : 'documenttitle'},
               2 : {'name' : 'costs-assessment-scheme', 'file' : 'costfile'}})


# In[23]:


#defsub


# In[24]:


#formsub


# In[25]:


#createdb('imotzza')


# In[26]:


#pushbutton('imotzza', 'wcm', '0135')


# In[27]:


#rdmot = readata('imotzza')


# In[28]:


#rdmot = readata('')


# In[29]:


#rdmot = readata('imoti')


# In[30]:


#rdmore


# In[ ]:





# In[31]:


#codelis = list()
#for rdm in rdmot[0][2]:
#    codelis.append(int(rdm))
#whatcodes(codelis)


# In[ ]:





# In[32]:


#readone(namedatabase)


# In[33]:


def rdmore(database):
    rdmot = readone(database)
    codelis = list()
    for rdm in rdmot[0][2]:
        codelis.append(int(rdm))
    return(whatcodes(codelis))


# In[38]:


#rdrecent = readone('imoti')


# In[42]:


#rdrecent[0][2]


# In[88]:


#rdrecent = readone('imoti')


# In[96]:


#for rdr in list(str(rdrecent[0][2])):
    #print(rdr)
    #print(objda[int(rdr)])


# In[94]:


#objda[0]


# In[97]:


def wspl(): 
    rdrecent = readone('imoti')
    #whatcodes(list(str(rdrecent[0][2])))
    #return(list(str(rdrecent[0][2])))
    
    for rdr in list(str(rdrecent[0][2])):
        #print(rdr)
        print(objda[int(rdr)])
    
        
        #print(rdr)



    #return(rdrecent)

    #for wspl in whatcodes.split(','):
    #    print(wspl)
#    print(objda[int(wspl)])
#    resultdic.update({})


# In[98]:


#wspl()


# In[47]:


#rdrecent = readone('imoti')


# In[51]:


#rdrecent[0][2]


# In[ ]:





# In[ ]:





# In[45]:


def rdthis(database):
    rdrecent = readone('imoti')
    return(whatcodes(codelis))
    #readone(namedatabase)


# In[46]:


#rdthis('imoti')


# In[ ]:





# In[181]:


#rdmore()

